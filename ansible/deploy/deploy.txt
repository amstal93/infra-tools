###############################
#       Static Inventory      #
###############################
##### DocKube #####
### Master
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_nsq-nsqadmin.yaml --limit "172.212.0.6"
### Node
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_nsq-nsqd.yaml --limit "172.212.0.8"
### NFS
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_nsq-nsqlookupd.yaml --limit "172.212.0.11"
### All
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_nsq-nsqadmin.yaml
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_nsq-nsqd.yaml
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_nsq-nsqlookupd.yaml
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_golang.yaml
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_docker.yaml

##### {{datacenter}} #####
ansible-playbook -i inventories/{{datacenter}}/inventory.ini playbooks/tag_nsq-nsqadmin.yaml
ansible-playbook -i inventories/{{datacenter}}/inventory.ini playbooks/tag_nsq-nsqd.yaml
ansible-playbook -i inventories/{{datacenter}}/inventory.ini playbooks/tag_nsq-nsqlookupd.yaml
ansible-playbook -i inventories/{{datacenter}}/inventory.ini playbooks/tag_golang.yaml
ansible-playbook -i inventories/{{datacenter}}/inventory.ini playbooks/tag_docker.yaml

##### Aliyun (Alibaba Cloud) #####
ansible-playbook -i inventories/aliyun/inventory.ini playbooks/tag_nsq-nsqadmin.yaml
ansible-playbook -i inventories/aliyun/inventory.ini playbooks/tag_nsq-nsqd.yaml
ansible-playbook -i inventories/aliyun/inventory.ini playbooks/tag_nsq-nsqlookupd.yaml
ansible-playbook -i inventories/aliyun/inventory.ini playbooks/tag_golang.yaml
ansible-playbook -i inventories/aliyun/inventory.ini playbooks/tag_docker.yaml

##### GCP #####
ansible-playbook -i inventories/gcp/inventory.ini playbooks/tag_nsq-nsqadmin.yaml
ansible-playbook -i inventories/gcp/inventory.ini playbooks/tag_nsq-nsqd.yaml
ansible-playbook -i inventories/gcp/inventory.ini playbooks/tag_nsq-nsqlookupd.yaml
ansible-playbook -i inventories/gcp/inventory.ini playbooks/tag_golang.yaml
ansible-playbook -i inventories/gcp/inventory.ini playbooks/tag_docker.yaml


###############################
#      Dynamic Inventory      #
###############################
##### {{datacenter}} #####
ansible-playbook -i inventories/{{datacenter}}/ playbooks/tag_docker.yaml

##### Aliyun (Alibaba Cloud) #####
ansible-playbook -i inventories/aliyun/alicloud.py playbooks/tag_docker.yaml
ansible-playbook -i inventories/aliyun/alicloud.py playbooks/tag_nsq-nsqadmin.yaml
ansible-playbook -i inventories/aliyun/alicloud.py playbooks/tag_nsq-nsqd.yaml
ansible-playbook -i inventories/aliyun/alicloud.py playbooks/tag_nsq-nsqlookupd.yaml
ansible-playbook -i inventories/aliyun/alicloud.py playbooks/tag_golang.yaml

##### GCP #####
ansible-playbook -i inventories/gcp/gce.py playbooks/tag_docker.yaml
ansible-playbook -i inventories/gcp/gce.py playbooks/tag_nsq-nsqadmin.yaml
ansible-playbook -i inventories/gcp/gce.py playbooks/tag_nsq-nsqd.yaml
ansible-playbook -i inventories/gcp/gce.py playbooks/tag_nsq-nsqlookupd.yaml
ansible-playbook -i inventories/gcp/gce.py playbooks/tag_golang.yaml

##### CloudRaya #####
ansible-playbook -i inventories/cloudraya/inventory.ini playbooks/tag_nsq-nsqadmin.yaml
ansible-playbook -i inventories/cloudraya/inventory.ini playbooks/tag_nsq-nsqd.yaml
ansible-playbook -i inventories/cloudraya/inventory.ini playbooks/tag_nsq-nsqlookupd.yaml
ansible-playbook -i inventories/cloudraya/inventory.ini playbooks/tag_golang.yaml
ansible-playbook -i inventories/cloudraya/inventory.ini playbooks/tag_docker.yaml
ansible-playbook -i inventories/cloudraya/inventory.ini playbooks/cloudraya_goapp.yaml --become-user root -u root --ask-pass
ansible-playbook -i inventories/cloudraya/inventory.ini playbooks/cloudraya_goapp.yaml --private-key keys/id_rsa