# **Infra Tools**

Collection sets of tools for provision & deployment instances (VM) & container.

## **Features**
* Ansible
* Dockerframework
* DocKube
* HelmCharts
* Kubespray (K8S Deployment)
* Nomad
* Packer
* Portainer (Docker UI Tools)
* Terragrunt (Terraform Wrapper)
* Vault

## **How To Run Ansible**
* Goto folder `ansible`

```
cd ansible
```

* Setup `remote_user` in `ansible/ansible.cfg`

```
remote_user=root  # root / ubuntu
```

* Deploy Ansible Playbook

```
###############################
#       Static Inventory      #
###############################
##### DocKube #####
### Master
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_nsq-nsqadmin.yaml --limit "172.212.0.6"
### Node
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_nsq-nsqd.yaml --limit "172.212.0.8"
### NFS
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_nsq-nsqlookupd.yaml --limit "172.212.0.11"
### All
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_nsq-nsqadmin.yaml
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_nsq-nsqd.yaml
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_nsq-nsqlookupd.yaml
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_golang.yaml
ansible-playbook -i inventories/dockube/inventory.ini playbooks/dockube_docker.yaml

##### {{datacenter}} #####
ansible-playbook -i inventories/{{datacenter}}/inventory.ini playbooks/tag_nsq-nsqadmin.yaml
ansible-playbook -i inventories/{{datacenter}}/inventory.ini playbooks/tag_nsq-nsqd.yaml
ansible-playbook -i inventories/{{datacenter}}/inventory.ini playbooks/tag_nsq-nsqlookupd.yaml
ansible-playbook -i inventories/{{datacenter}}/inventory.ini playbooks/tag_golang.yaml
ansible-playbook -i inventories/{{datacenter}}/inventory.ini playbooks/tag_docker.yaml

##### Aliyun (Alibaba Cloud) #####
ansible-playbook -i inventories/aliyun/inventory.ini playbooks/tag_nsq-nsqadmin.yaml
ansible-playbook -i inventories/aliyun/inventory.ini playbooks/tag_nsq-nsqd.yaml
ansible-playbook -i inventories/aliyun/inventory.ini playbooks/tag_nsq-nsqlookupd.yaml
ansible-playbook -i inventories/aliyun/inventory.ini playbooks/tag_golang.yaml
ansible-playbook -i inventories/aliyun/inventory.ini playbooks/tag_docker.yaml

##### GCP #####
ansible-playbook -i inventories/gcp/inventory.ini playbooks/tag_nsq-nsqadmin.yaml
ansible-playbook -i inventories/gcp/inventory.ini playbooks/tag_nsq-nsqd.yaml
ansible-playbook -i inventories/gcp/inventory.ini playbooks/tag_nsq-nsqlookupd.yaml
ansible-playbook -i inventories/gcp/inventory.ini playbooks/tag_golang.yaml
ansible-playbook -i inventories/gcp/inventory.ini playbooks/tag_docker.yaml

##### CloudRaya #####
ansible-playbook -i inventories/cloudraya/inventory.ini playbooks/tag_nsq-nsqadmin.yaml
ansible-playbook -i inventories/cloudraya/inventory.ini playbooks/tag_nsq-nsqd.yaml
ansible-playbook -i inventories/cloudraya/inventory.ini playbooks/tag_nsq-nsqlookupd.yaml
ansible-playbook -i inventories/cloudraya/inventory.ini playbooks/tag_golang.yaml
ansible-playbook -i inventories/cloudraya/inventory.ini playbooks/tag_docker.yaml
ansible-playbook -i inventories/cloudraya/inventory.ini playbooks/cloudraya_goapp.yaml --become-user root -u root --ask-pass
ansible-playbook -i inventories/cloudraya/inventory.ini playbooks/cloudraya_goapp.yaml --private-key keys/id_rsa


###############################
#      Dynamic Inventory      #
###############################
##### {{datacenter}} #####
ansible-playbook -i inventories/{{datacenter}}/ playbooks/tag_docker.yaml

##### Aliyun (Alibaba Cloud) #####
ansible-playbook -i inventories/aliyun/alicloud.py playbooks/tag_docker.yaml
ansible-playbook -i inventories/aliyun/alicloud.py playbooks/tag_nsq-nsqadmin.yaml
ansible-playbook -i inventories/aliyun/alicloud.py playbooks/tag_nsq-nsqd.yaml
ansible-playbook -i inventories/aliyun/alicloud.py playbooks/tag_nsq-nsqlookupd.yaml
ansible-playbook -i inventories/aliyun/alicloud.py playbooks/tag_golang.yaml

##### GCP #####
ansible-playbook -i inventories/gcp/gce.py playbooks/tag_docker.yaml
ansible-playbook -i inventories/gcp/gce.py playbooks/tag_nsq-nsqadmin.yaml
ansible-playbook -i inventories/gcp/gce.py playbooks/tag_nsq-nsqd.yaml
ansible-playbook -i inventories/gcp/gce.py playbooks/tag_nsq-nsqlookupd.yaml
ansible-playbook -i inventories/gcp/gce.py playbooks/tag_golang.yaml
```

## Container Application
### Create Docker Image
* Docker Build
  ```
  docker build . -t zeroc0d3/[IMAGE]:[TAG]
  ```
* Push Docker Image
  ```
  docker push zeroc0d3/[IMAGE]:[TAG]
  ```
* Shorcut Builder & Push to Docker Registry
  ```
  make push-container
  ```

## Development
### Docker Compose
* Copy environment file
  ```
  cp .env.example .env
  ```
* Running Docker-Compose
  ```
  ./run-docker.sh
  -- or --
  make run-docker
  ```

## Testing Inside Container
* List docker running
  ```
  docker ps
  ---
  CONTAINER ID        IMAGE                                COMMAND                  CREATED             STATUS              PORTS                    NAMES
  aaabff1d67a9        zeroc0d3/golang-bookstore            "/bin/sh"                3 minutes ago       Up 3 minutes        0.0.0.0:8080->8080/tcp   zeroc0d3lab_golang
  ```
* If you can't see container `zeroc0d3lab_golang`, re-run again:
  ```
  make run-docker
  ```
* Running application inside container
  ```
  docker exec -it zeroc0d3lab_golang ash
  ---
  go run main.go
  ```

## API Test
* Get Books
```
GET    : /books
         curl --request GET \
            --url 'http://localhost:8080/books' \
            --header 'Content-Type: application/json' | jq
```

* Add Book 1
```
POST   : /books
         curl --request POST \
            --url 'http://localhost:8080/books' \
            --header 'Content-Type: application/json' \
            --data '{
                "title": "Mastering Go: Create Golang production applications using network libraries, concurrency, and advanced Go data structures",
                "author": "Mihalis Tsoukalos"
            }' | jq
```

* Add Book 2
```
POST   : /books
         curl --request POST \
            --url 'http://localhost:8080/books' \
            --header 'Content-Type: application/json' \
            --data '{
                "title": "Introducing Go: Build Reliable, Scalable Programs",
                "author": "Caleb Doxsey"
            }' | jq
```

* Add Book 3
```
POST   : /books
         curl --request POST \
            --url 'http://localhost:8080/books' \
            --header 'Content-Type: application/json' \
            --data '{
                "title": "Learning Functional Programming in Go: Change the way you approach your applications using functional programming in Go",
                "author": "Lex Sheehan"
            }' | jq
```

* Edit Book 3
```
PATCH   : /books/3
         curl --request PATCH \
            --url 'http://localhost:8080/books/3' \
            --header 'Content-Type: application/json' \
            --data '{
                "title": "Test Golang",
                "author": "ZeroC0D3Lab"
            }' | jq
```

* Delete Book 3
```
DELETE   : /books/3
         curl --request DELETE \
            --url 'http://localhost:8080/books/3' \
            --header 'Content-Type: application/json' | jq
```

## PoC Development microk8s
* **Dashboard**
  ![dashboard](img/golang-bookstore/000.png)
* **Namespace**
  ![namespace](img/golang-bookstore/001.png)
* **Persistant Volume**
  ![pvc](img/golang-bookstore/002.png)
* **ConfigMap**
  ![configmap](img/golang-bookstore/003.png)
* **Deployment**
  ![deployment](img/golang-bookstore/004.png)
* **Service**
  ![service](img/golang-bookstore/005.png)
* **Replication**
  ![service](img/golang-bookstore/006.png)
* **GET** /books (blank)
  ![service](img/golang-bookstore/007.png)
* **POST** /books
  ![service](img/golang-bookstore/008.png)
* **GET** /books (after insert)
  ![service](img/golang-bookstore/009.png)


## Copyright
* Author : **Dwi Fahni Denni (@zeroc0d3)**
* License: **Apache ver-2**